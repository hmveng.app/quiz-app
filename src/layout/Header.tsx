import { Outlet} from "react-router-dom";
import Container from 'react-bootstrap/Container';
import Stack from 'react-bootstrap/Stack';
import Navbar from 'react-bootstrap/Navbar';
import { useContext } from 'react';

import { AuthContext } from '../context/AuthContext';

export default function Header() {

    const {username} = useContext(AuthContext);

    return (
        <Container fluid="md">
            <Stack direction="vertical" gap={3}>
                <Navbar className="bg-body-tertiary">
                    <Container fluid="md" >
                        <Navbar.Brand className="fs-3">Quiz App</Navbar.Brand>
                        <Navbar.Toggle />
                        {
                            username && (
                                <>
                                    <Navbar.Collapse className="justify-content-end">
                                        <Navbar.Text>
                                            Signed in as <b>{username}</b>
                                        </Navbar.Text>
                                    </Navbar.Collapse>
                                </>
                            )
                        }
                    </Container>
                </Navbar>
            </Stack>

            {/* Other pages */}
            <Outlet />

        </Container>
    )
}