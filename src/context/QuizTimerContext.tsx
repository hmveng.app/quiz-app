import { createContext, useState, useEffect, useRef, useContext, useCallback } from 'react';

import RemainingTime from '../component/RemainingTime';
import { ProcessEvalContext } from '../context/QuestionsContext';

export const QuizTimerContext = createContext<{
  clearQuizTimer: (value:boolean) => void;
  timeMadeToRespond: number,
  remainingTimeResponse: number
}>({
  clearQuizTimer: () => {},
  timeMadeToRespond: 0,
  remainingTimeResponse: 0
});


interface Props {
    children: React.ReactNode
}

export default function QuizTimerProvider({children}:Props) {
    const {hasNextQuestion} = useContext(ProcessEvalContext);

    const MAX_DURATION = 90000; // 90 seconds => 1.5 munutes
    const [startTime, setStartTime] = useState(Date.now());
    const [currentTime, setCurrentTime] = useState(0);
    const intervalRef = useRef<any>(0);

    const handleStop = useCallback((init:boolean = false) => {
        clearInterval(intervalRef.current);

        // if action come from user clicking
        if(init === true && hasNextQuestion === true) {
          setStartTime(Date.now());
        }
      }, [hasNextQuestion]);

    useEffect(() => {
        setCurrentTime(Date.now());
    
        handleStop();

        intervalRef.current = setInterval(() => {
          setCurrentTime(Date.now());
        }, 10);

        return () => {
          handleStop(true)
        };
    }, [startTime, handleStop]);

    let secondsPassed = 0;
    if (startTime != null && currentTime != null) {
      secondsPassed = (currentTime - startTime);
    }

    let timeLeft = parseInt(((MAX_DURATION - secondsPassed) /1000).toFixed());
    let duration = (MAX_DURATION/1000) - timeLeft;

    return (
      <QuizTimerContext.Provider 
        value={{
          clearQuizTimer: handleStop,
          timeMadeToRespond: duration,
          remainingTimeResponse: timeLeft
        }}
      >
        <RemainingTime timeLeft={timeLeft} maxDuration={MAX_DURATION} timeLeftWarningInSeconds={20} />
        {children}
      </QuizTimerContext.Provider>
    )
}