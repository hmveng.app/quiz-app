import { createContext, useState } from 'react';


import useData from '../middleware/useData';
import Question from '../models/question'
import ProcessEval from '../models/process-eval';
import ArrayUtil from '../utils/array-util';

export const QuestionsContext = createContext<Question[]>([]);
export const QuestionContext = createContext<{
        question: Question|null,
        onNext: () => void;
    }>({
        question: null,
        onNext: () => {}
    });
export const ProcessEvalContext = createContext<ProcessEval>({
    position: 0,
    total: 0,
    hasNextQuestion: true
});


interface Props {
    children: React.ReactNode
}

export function QuestionsProvider({children}: Props) {
    const [position, setPosition] = useState<number>(0);

    let questions: Question[] = useData("api/questions.json");
        
    if(position === 0) {
        questions = ArrayUtil.shuffleQuestions(questions);
    }

    const nextQuestion = () => {
         if(position < questions.length) {
            setPosition(position+1);
         }
    }

    return (
        <QuestionContext.Provider 
            value={{
                question: questions[position],
                onNext: nextQuestion
            }} 
        >
            <ProcessEvalContext.Provider 
                value={{
                    position: position + 1 ,
                    total: questions.length,
                    hasNextQuestion: (position + 1) < questions.length 
                }}
            >
                {children}
            </ProcessEvalContext.Provider>
        </QuestionContext.Provider>
    )
}