import { createContext, useState } from 'react';
import { useNavigate } from "react-router-dom";

interface Player {
   isAuth: boolean,
   username: string|null
   onSetAuth: (e: string|null) => void;
}

export const AuthContext = createContext<Player>({ isAuth: false, username: null, onSetAuth: () => {}});


interface Props {
    children: React.ReactNode
}

export default function AuthProvider({children}: Props) {
    const [playername, setPlayername] = useState<string|null>(null);
    const [isAuth, setIsAuth] = useState<boolean>(false);
    const navigate = useNavigate();

    const onHandleAuthentication = (username:string|null) => {
        console.log('[username] ', username)
        const currentPlayer = username !== null ? username : null;
        const isAuth = username !== null;
        const routePath = isAuth ? "quiz" : "/";

        setPlayername(currentPlayer);
        setIsAuth(isAuth);

        navigate(routePath);
    }

    return (
        <AuthContext.Provider 
            value={{
                isAuth: isAuth,
                username: playername,
                onSetAuth: onHandleAuthentication
            }} 
        >
            {children}
        </AuthContext.Provider>
    )
}