import { useContext } from "react";
import { Navigate, Outlet } from "react-router-dom";

import { AuthContext } from '../context/AuthContext';

const ProtectedRoute = () => {
    const {isAuth} = useContext(AuthContext);

  if (isAuth === false) return <Navigate to="/" />;

  return <Outlet />;
};

export default ProtectedRoute;