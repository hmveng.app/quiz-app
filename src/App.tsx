import React from 'react';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import './App.scss';

import Home from './pages/Home';
import QuizPage from './pages/QuizPage';
import Header from './layout/Header';
import AuthProvider from './context/AuthContext';
import ProtectedRoute from './router/ProtectedRoute';

function App() {

  return (
    <BrowserRouter>
      <AuthProvider>
        <Routes>
          <Route path="/" element={<Header />}>
            <Route index element={<Home />} />
            <Route element={<ProtectedRoute />}>
              <Route path="quiz" element={<QuizPage />} />
            </Route>
            <Route path="*" element={<Home />} />
          </Route>
        </Routes>
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
