import Answer from '../models/answer';
import Solution from '../models/solution';

interface Result {
    note: string, 
    totalQuestion: number,
    hasWin: boolean,
    result: number,
    time: string
}

interface cResult {
    result: number,
    time: number
}

export default class ComputeUtil {

    static compute(answers: Answer[], solutions: Solution[]): Result {
        const cResult:cResult = answers.reduce((prevResult:cResult, currentAnswser:Answer) => {
            // Get solution
            const solution = solutions.find((item: Solution) => item.id === currentAnswser.questionId);
            
            let point = 0;
            if(solution?.values.length === currentAnswser.questionAnswers.length) {
                // If every values of solution are present on answer
                point = solution?.values.every((s:string) => currentAnswser.questionAnswers.includes(s)) ?
                     1 : 0;
             }

            return {
                result: prevResult.result + point,
                time: prevResult.time + currentAnswser.timing
            }

        }, { result: 0, time: 0});

        const hasWin = ComputeUtil.hasWin(cResult.result, answers.length)

        return {
            note: ComputeUtil.noteFormat(hasWin), 
            totalQuestion: answers.length,
            hasWin: hasWin,
            result: cResult.result,
            time: ComputeUtil.formatTime(cResult.time) // in minutes
        }
    }

    private static hasWin(result: number, totalItems: number) {
        const minResult = totalItems * 0.8;

        return  result >= minResult;
    }

    private static noteFormat(hasWin: boolean) {
        return hasWin ? 'Congratulations' : 'Not enough';
    }


    private static formatTime(time: number): string {
        const minutes = Math.floor(time / 60);
        const seconds = time > 59 ? Math.floor(time / 360) : time;

        if (minutes > 0) {
            return seconds > 0 ? `in ${minutes} min ${seconds}s` : `in ${minutes} min`;
        }

        return `in ${seconds}s`;
    }


}