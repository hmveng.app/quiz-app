import Question from '../models/question';
import { QResponse} from '../models/question-response';

export default class ArrayUtil {

    static shuffleQuestions(data: Question[]): Question[] {
        const randomQuestions = ArrayUtil.randomArray<Question>(data);

        return randomQuestions.map((question: Question) => {
                return {
                    ...question,
                    responses: ArrayUtil.randomArray<QResponse>(question.responses)
                }
            });
    }

    private static randomArray<T>(data: T[]) {
        const randomPos = ArrayUtil.generateRandomPositionArray(data.length, []);
        const randomData = Array(data.length).fill(null);
        
        for (var i = 0; i < data.length; i++) {
            const j = randomPos[i];
            randomData[j] = data[i];
        }

        return randomData;
    }

    private static generateRandomPositionArray(max: number, positions: number[]): number[] {
           if(positions.length === max) {
             return positions;
           } else {
              const pos = Math.floor(Math.random() * max);
              return positions.includes(pos) ?
                ArrayUtil.generateRandomPositionArray(max, positions) : 
                ArrayUtil.generateRandomPositionArray(max, [...positions, pos]);
           }
    }

}