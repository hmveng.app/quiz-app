

export default interface Answer {
    questionId: string|undefined, 
    questionAnswers: string[],
    timing: number
}