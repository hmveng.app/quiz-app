

export default interface ProcessEval {
    position: number,
    total: number
    hasNextQuestion: boolean
}