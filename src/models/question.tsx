
import { QResponse } from './question-response';

export default interface Question {
    id: string,
    title: string,
    responses: QResponse[],
    isMultiple: boolean
}