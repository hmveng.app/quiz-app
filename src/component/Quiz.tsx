import { useContext, useState, useRef, useEffect } from 'react';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import { QuestionContext, ProcessEvalContext } from '../context/QuestionsContext';
import { QuizTimerContext } from '../context/QuizTimerContext';
import Answer from '../models/answer';


interface Props {
    onReceiveAnswer: (data: Answer, isComplete: boolean) => void;
}

export default function Quiz({onReceiveAnswer}:Props) {
    const {question, onNext} = useContext(QuestionContext);
    const {hasNextQuestion} = useContext(ProcessEvalContext);
    const {clearQuizTimer, timeMadeToRespond, remainingTimeResponse} = useContext(QuizTimerContext);

    const btnNextRef = useRef<HTMLButtonElement>(null);

    const [checkedItems, setCheckedItems] = useState<string[]>([]);

    // event click btn for (un)select answsers
    const onChangeHandle = (value:string) => {
        let newItems = [];
        if(question?.isMultiple === true) {
            // checkboxes input case
            const items = checkedItems.filter( (item: string) => item === value);
            newItems = items.length !== 0 ? 
                checkedItems.filter( (item: string) => item !== value) : [...checkedItems, value];
        } else {
            // radio input case
            newItems = [value];
        }

        setCheckedItems(newItems);
    }

    // event click btn for next question
    const onNextHandle = () => {
        onReceiveAnswer({
                questionId: question?.id, 
                questionAnswers: checkedItems,
                timing: timeMadeToRespond
            }, 
            hasNextQuestion === false
        );
        
        // go to the next question and reset Timer
        onNext();
        clearQuizTimer(true);
        setCheckedItems([]);
    }

    // if remainingTimeResponse is 0, automatically we go on the next question
    useEffect(() => {
       if(remainingTimeResponse === 0) {
         btnNextRef.current?.click();
       }
    }, [remainingTimeResponse]);

    return (
        <>
            <div className='my-1'>
                <span className='fw-bolder fs-6'>Statement : </span>
                {question?.title}
            </div>
            {
                question?.responses.map((response) => {
                    return (
                        <Form.Check
                            key={`input-${response.id}`}
                            name={`group-${question?.id}`}
                            type={ !!question?.isMultiple ? 'checkbox' : 'radio'}
                            value={response.id}
                            label={response.label}
                            onChange={(event) => onChangeHandle(event.target.value) }
                        />
                    )
                })
            }
            <div className='w-100 mt-2'>
            { question?.isMultiple === true && (<span className='mt-2 fs-6 fst-italic text-primary'>/!\ Multiple choices.</span>) }
                <Button 
                    variant="primary" 
                    className='float-end'
                    onClick={onNextHandle}
                    ref={btnNextRef}
                    disabled={timeMadeToRespond < 1}
                >
                    { hasNextQuestion ? 'Next' : 'Complete' }
                </Button>
            </div>
        </>
    )

}