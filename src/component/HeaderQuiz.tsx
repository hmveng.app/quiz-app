import { useContext } from 'react';

import { ProcessEvalContext } from '../context/QuestionsContext';
import ProcessEval from '../models/process-eval';

export default function HeaderQuiz() {
    const context:ProcessEval|null = useContext(ProcessEvalContext);

    return (
        <div className='my-4 text-uppercase fw-bold pb-4 text-center'>
            <div>Question {context?.position} / {context?.total} </div>

            <progress value={context?.position} max={context?.total} />
            
        </div>
    )
}