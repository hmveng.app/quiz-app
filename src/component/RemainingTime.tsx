


interface Props {
   timeLeft: number,
   maxDuration: number,
   timeLeftWarningInSeconds: number,
}

export default function RemainingTime ({timeLeft, maxDuration, timeLeftWarningInSeconds}: Props) {

    let maxDurationInSec = maxDuration/1000;
    let maxDurationInMinutes = maxDuration/60000;

    let className = "float-end";
    if(timeLeft <= timeLeftWarningInSeconds) {
        className += " text-danger border border-2 border-danger px-3 rounded";
    } else {
        className += " text-success"
    }

    return (
        <div className="text-center mb-2">
            <span className='float-start'>
                Time limit {maxDurationInSec}s ({maxDurationInMinutes} min)
            </span>
            <span className={className}>
                You have <b>{timeLeft}</b> seconds left 
            </span>
        </div>
    )

}