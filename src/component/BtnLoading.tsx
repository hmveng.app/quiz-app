import Button from 'react-bootstrap/Button';
import Spinner from 'react-bootstrap/Spinner';

interface Props {
    onHandleClick: () => void;
    isLoading: boolean,
    title: string
}

export default function BtnLoading({isLoading, title, onHandleClick}:Props) {

    let spinnerItem = null;
    if (isLoading === true) {
        spinnerItem = <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true"/> ;
    }

    return (
        <Button variant="primary" onClick={onHandleClick} disabled={isLoading}>
            {spinnerItem}
            { isLoading ? ' Loading...' : title }
        </Button>
    )
}