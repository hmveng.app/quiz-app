import { useContext, useState } from 'react';
import Button from 'react-bootstrap/Button';
import Stack from 'react-bootstrap/Stack';

import useData from '../middleware/useData';
import Solution from '../models/solution';
import ComputeUtil from '../utils/compute-util';
import Answer from '../models/answer';
import { AuthContext } from '../context/AuthContext';
import BtnLoading from '../component/BtnLoading';

interface Props {
    answers: Answer[]
}

export default function Resume({answers}: Props) {
    const {onSetAuth} = useContext(AuthContext);
    const solutions: Solution[] = useData("api/solutions.json");

    const [isLoading, setIsLoading] = useState<boolean>(false);

    const {note, totalQuestion, result, time, hasWin} = ComputeUtil.compute(answers, solutions);

    const className = hasWin ? 'text-success': 'text-danger';

    const onHandleClick = () => {
        setIsLoading(true);
        setTimeout(() => {
            onSetAuth(null);
            setIsLoading(false);
        }, 1000);
    }

    return (
        <>
            <Stack gap={2} className="col-md-6 mx-auto mt-5 py-3">
                <div className="my-4">
                    <div className='text-uppercase fw-bold pb-4 text-center'>Quiz completed!</div>
                    <div className='text-center my-2 fs-3'>
                        <span className={className}>
                            {note}
                        </span> 
                    </div>
                    <div className='text-center'>
                        You scored { result } / { totalQuestion } on this quiz { time }. Thank you for your participation.
                    </div>

                </div>

                <div className='w-100 mt-2 text-center'>
                    <BtnLoading 
                        isLoading={isLoading}
                        title='Home'
                        onHandleClick={onHandleClick}
                    />
                </div>
            </Stack>
        </>
    )
}