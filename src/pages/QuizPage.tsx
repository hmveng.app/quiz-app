import { useState } from 'react';

import Stack from 'react-bootstrap/Stack';

import { QuestionsProvider } from '../context/QuestionsContext';
import Quiz from '../component/Quiz';
import HeaderQuiz from '../component/HeaderQuiz';
import QuizTimerProvider from '../context/QuizTimerContext';
import Answer from '../models/answer';
import Resume from '../component/Resume';


export default function QuizPage() {
    const [quizIsComplete, setQuizIsComplete] = useState(false);
    const [answers, setAnswers] = useState<Answer[]>([]);

    const onHandleReceiveAnswer = (answer: Answer, isComplete: boolean) => {
        if(isComplete === true) {
          setQuizIsComplete(true);
        }
        
        setAnswers([
            ...answers,
            answer
        ]);

    }
    

    if (quizIsComplete) {
        return <Resume answers={answers}/>
    }

    return (
        <Stack gap={2} className="col-md-10 mx-auto mt-5 py-3">

            <QuestionsProvider>
                <HeaderQuiz />
                <QuizTimerProvider>
                    <Quiz onReceiveAnswer={onHandleReceiveAnswer} />
                </QuizTimerProvider>
            </QuestionsProvider>

        </Stack>
    )
}