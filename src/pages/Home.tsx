import { useState, useContext } from 'react';
import Stack from 'react-bootstrap/Stack';
import Form from 'react-bootstrap/Form';

import BtnLoading from '../component/BtnLoading';
import { AuthContext } from '../context/AuthContext';

export default function Home() {
    const {onSetAuth} = useContext(AuthContext);
    const [hasError, setHasError] = useState(false);
    const [isLoading, setIsLoading] = useState(false);
    const [username, setUsername] = useState<string|null>(null);

    const handleQuizStart = () => {
        setIsLoading(true);
        setTimeout(() => {
            if (username !== null && username.trim() !== '' && username.trim().length > 2) {
                setHasError(false);
                // connexion....
                onSetAuth(username);
            } else {
                setHasError(true);
            }

            setIsLoading(false);

        }, 1000 );

        
    };

    return (
        <Stack gap={2} className="col-md-5 mx-auto mt-5 py-3">
            <div className="my-4 text-center text-uppercase fs-4">
               Welcome to the quiz
            </div>

            <div className="my-4 text-center fw-lighter fs-5">
                Enter your username to play.<br/>
                We remind you that once the quiz has started, it is impossible to go back to the beginning.<br/>
                If you don't answer a question within the specified time, the next question will be displayed.
            </div>

            <Form.Group className="mb-2">
                <Form.Control
                    required
                    type="text"
                    placeholder="Enter your username"
                    onChange={(e) => setUsername(e.target.value)}
                />
                { hasError && (
                        <Form.Text muted >
                            <span className='text-danger' >Your username is required or must be at least 3 characters.</span>
                        </Form.Text>
                    )
                 }
            </Form.Group>

            <BtnLoading 
              isLoading={isLoading}
              title='Start quiz'
              onHandleClick={handleQuizStart}
            />
            
        </Stack>
    )
}